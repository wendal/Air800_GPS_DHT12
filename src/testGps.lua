--- 模块功能：GPS功能测试.
-- @author openLuat
-- @module gps.testGps
-- @license MIT
-- @copyright openLuat
-- @release 2018.03.23

module(...,package.seeall)

require"gps" -- 基础gps模块
require"agps" -- 联网下载星历,加速定位

-- 定义一个变量,保存gps和dht12数据
RT = {}

-- 定时更新GPS数据
local function update_GPS()
    if gps.isOpen() then
        -- 打印一下GPS信息,方便调试
        local tLocation = gps.getLocation()
        local speed = gps.getSpeed()
        log.info("testGps.printGps",
            gps.isOpen(),gps.isFix(),
            tLocation.lngType,tLocation.lng,tLocation.latType,tLocation.lat,
            gps.getAltitude(),
            speed,
            gps.getCourse(),
            gps.getViewedSateCnt(),
            gps.getUsedSateCnt())
		-- 将数据逐个存入
		RT["gpsFix"] = gps.isFix() -- 是否定位成功
		RT["lngType"] = tLocation.lngType
		RT["lng"]     = tLocation.lng -- 纬度
		RT["latType"] = tLocation.latType
		RT["lat"]     = tLocation.lat -- 经度
		RT["altitude"] = gps.getAltitude()
		RT["speed"] = speed -- 速度
		RT["course"] = gps.getCourse()
		RT["viewedSateCnt"] = gps.getViewedSateCnt() -- 可见卫星数
		RT["usedSateCnt"] = gps.getUsedSateCnt() -- 使用的卫星数
    end
end


--[[
函数名：nemacb
功能  ：NEMA数据的处理回调函数
参数  ：
		data：一条NEMA数据
返回值：无
]]
local function nmeaCb(nmeaItem)
    log.info("testGps.nmeaCb",nmeaItem)
end

--设置GPS+BD定位
--如果不调用此接口，默认也为GPS+BD定位
--gps.setAerialMode(1,1,0,0)

--设置仅gps.lua内部处理NEMA数据
--如果不调用此接口，默认也为仅gps.lua内部处理NEMA数据
--如果gps.lua内部不处理，把NMEA数据通过回调函数cb提供给外部程序处理，参数设置为1,nmeaCb
--如果gps.lua和外部程序都处理，参数设置为2,nmeaCb
gps.setNmeaMode(2,nmeaCb)

-- 初始化并打开I2C操作DHT12
local function update_dht12()
    local id = 2
    if i2c.setup(id, i2c.SLOW) ~= i2c.SLOW then
        log.error("I2C.init is: ", "fail")
        i2c.close(id)
        return
    end
    i2c.send(id, 0x5C, 0x00)
    local data = i2c.recv(id, 0x5C, 5)
    i2c.close(id)
    log.info("DHT12 HEX data: ", data:toHex())
    -- 分别是湿度整数,湿度小数,温度整数,温度湿度
    local _, h_H, h_L, t_H, t_L = pack.unpack(data, 'b4')
    if h_H == nil then
        log.info("NO DHT12 data")
        return
    end
    log.info("DHT12 data: ", h_H, h_L, t_H, t_L)
    -- TODO 需要考虑温度低于0度的情况
    RT["ham"] = h_H .. ".".. h_L
    RT["temp"] = t_H .. "." .. t_L
end


--启动socket客户端任务
sys.taskInit(
    function()
        local retryConnectCnt = 0
        while true do -- 循环之
            if not socket.isReady() then
                retryConnectCnt = 0
                --等待网络环境准备就绪，超时时间是5分钟
                sys.waitUntil("IP_READY_IND",300000)
            end
            
            if socket.isReady() then
                --创建一个socket tcp客户端
                local socketClient = socket.tcp()
                --阻塞执行socket connect动作，直至成功
                if socketClient:connect(TCP_HOST,TCP_PORT) then
                    retryConnectCnt = 0
                    -- 补充imei和iccid
					RT["imei"] = misc.getImei()
                    RT["iccid"] = sim.getIccid()
                    local jsondata = json.encode(RT)
                    if socketClient:send(jsondata .. "\r\n") then
                        result,data = socketClient:recv(1000)
                        if result then
                            --TODO：处理收到的数据data
                            log.info("socketTask.recv",data)
                        end
                    end
                else
                    retryConnectCnt = retryConnectCnt+1
                end
                
                --断开socket连接
                socketClient:close()
                if retryConnectCnt>=5 then link.shut() retryConnectCnt=0 end
                sys.wait(5000) -- 等待5秒,下一轮
            else
                --进入飞行模式，20秒之后，退出飞行模式, 尝试解决联网问题
                net.switchFly(true)
                sys.wait(20000)
                net.switchFly(false)
            end
        end
    end
)


-- 打开GPS
gps.open(gps.DEFAULT,{tag="TEST1",cb=update_GPS})
-- 定期更新GPS
sys.timerLoopStart(update_GPS,2000)
-- 定期更新DHT12
sys.timerLoopStart(update_dht12,2000)
